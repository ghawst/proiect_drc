const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(bodyParser.json());

const sequelize = new Sequelize('c9', 'ghawst', '', {
    host: 'localhost',
    dialect:'mysql',
    operatorsAliases: false
});

sequelize.authenticate().then(()=>{
    console.log('Connection to db realized successfully');
}).catch((err) =>{
    console.log(`Connection failed: ${err}`);
});

const Contact = sequelize.define('contacts', {
   firstName: {
       type: Sequelize.STRING,
       allowNull: false
   },
   lastName: {
      type: Sequelize.STRING,
      allowNull: false
   },
   email: {
       type: Sequelize.STRING,
      allowNull: false
   },
   phone: {
       type: Sequelize.INTEGER,
      allowNull: false
   },
   username: {
       type: Sequelize.STRING,
      allowNull: false
   }
});

const User = sequelize.define('users', {
   firstName: {
       type: Sequelize.STRING,
       allowNull: false
   },
   lastName: {
      type: Sequelize.STRING,
      allowNull: false
   },
   username: {
      type: Sequelize.STRING,
      allowNull: false
   },
   password: {
      type: Sequelize.STRING,
      allowNull: false
   },
   email: {
      type: Sequelize.STRING,
      allowNull: false
   }
});

sequelize.sync({force:true}).then(() => {
    console.log('Tables created!');
}).catch((err) => {
    console.log(`Failed to create tables`);
})

app.post('/add-contact', (req, res) => {
    Contact.create({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone,
        username: req.body.username
    }).then((contact) =>{
        res.status(200).send(contact);
    }).catch((err) => {
        res.status(500).send(err);
    });
});

app.put('/edit-contact', (req, res) => {
    Contact.update({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        phone: req.body.phone,
        username: req.body.username
    },{where: {id: req.body.id}}).then((contact) =>{
        res.status(200).send(contact);
    }).catch((err) => {
        res.status(500).send(err);
    });
});

app.delete('/delete-contact', (req, res) => {
    Contact.destroy({where:{id:req.body.id}}).then((contact) =>{
        res.status(200).send(contact);
    }).catch((err) => {
        res.status(500).send(err);
    });
});

app.get('/get-all-contacts', (req, res) => {
   Contact.findAll().then((contacts) => {
       res.status(200).send(contacts);
   }).catch((err) => {
       res.status(500).send(err);
   });
});

app.get('/get-user-contacts/:currentUser', (req, res) => {
   Contact.findAll({where:{username:req.params.currentUser}}).then((contacts) => {
       res.status(200).send(contacts);
   }).catch((err) => {
       res.status(500).send(err);
   });
});

app.post('/register', (req, res) => {
    User.findOne({where:{username:req.body.username}}).then((user) => {
       if(!user) {
           User.create({
            username: req.body.username,
            password: req.body.password,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email
        }).then((user) =>{
            res.status(200).send(user);
        }).catch((err) => {
            res.status(500).send(err);
        });
       }
    });
});

app.post('/login', (req, res) => {
   User.findOne({where:{username: req.body.username, password: req.body.password} }).then((user) => {
        res.status(200).send(user);
   }).catch((err) => {
        res.status(500).send(err);
   });
});

app.listen(8081, () => {
    console.log('Server started on port 8081...');
});