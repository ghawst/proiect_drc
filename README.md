In folder-ul 'drcapi' se afla baza de date. Aceasta poate fi pornita prin comanda 'node index.js'.
In folder-il 'drc-app' se afla aplicatia, lansata prin 'npm start'.
Momentan printr-un formular de 'Register' se pot adauga utilizatori, care pot fi gasiti prin formularul 'Login'.
De asemenea, prin formularul 'Add new contact' se pot adauga contacte noi, ce vor fi afisate in ordinea adaugarii in sectiunea 'Contact List'.
In momentul de fata, toate acestea se gasesc pe o singura pagina.