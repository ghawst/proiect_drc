import React from "react";
import axios from "axios";

export class AddContact extends React.Component {
    constructor(props) {
        super(props);
        this.state={};
        this.state.firstName = "";
        this.state.lastName = "";
        this.state.email = "";
        this.state.phone = 0;
    }
    
    handleChangeFirstName = (event) => {
        this.setState({
            firstName: event.target.value
        })
    }
    
    handleChangeLastName = (event) => {
        this.setState({
            lastName: event.target.value
        })
    }
    
    handleChangeEmail = (event) => {
        this.setState({
            email: event.target.value
        })
    }
    
    handleChangePhone = (event) => {
        this.setState({
            phone: event.target.value
        })
    }
    
    addContact = () => {
        if(window.currentUser !== "@") {
            let contact = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                email: this.state.email,
                phone: this.state.phone,
                username: window.currentUser
            }
            axios.post("https://proiectdrc-ghawst.c9users.io:8081/add-contact", contact).then((res) => {if(res.status === 200){this.props.contactAdded(contact);}}).catch((err) => {console.log(err)});
        } else {
            alert("Not logged in.");
        }
    }
    
    render() {
        return (
            <div>
                <h1>Add new contact</h1>
                <label htmlFor="fName">First Name:</label>
                <input className="contactInput" id="fName" type="text" placeholder="First Name" onChange={this.handleChangeFirstName} />
                <label htmlFor="lName">Last Name:</label>
                <input className="contactInput" id="lName" type="text" placeholder="Last Name" onChange={this.handleChangeLastName} />
                <label htmlFor="email">Email:</label>
                <input className="contactInput" id="email" type="email" placeholder="Email" onChange={this.handleChangeEmail} />
                <label htmlFor="mobTel">Mobile phone number:</label>
                <input className="contactInput" id="mobTel" type="tel" onChange={this.handleChangePhone} />
                <button id="addContact" onClick={this.addContact}>Add Contact</button>
            </div>
        );
    }
}