import React from "react";
import axios from "axios";

export class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state={};
        this.state.username = "";
        this.state.password = "";
    }
    
    handleChangeUsername = (event) => {
        this.setState({
            username: event.target.value
        })
    }
    
    handleChangePassword = (event) => {
        this.setState({
            password: event.target.value
        })
    }
    
    verifyUser = () => {
        let user = {
            username: this.state.username,
            password: this.state.password
        }
        axios.post("https://proiectdrc-ghawst.c9users.io:8081/login", user).then((res) => {if(res.status === 200){
            if(res.data.username) {
                alert("Logged in as " + res.data.username);
                window.currentUser = res.data.username;
                this.props.loggedIn();
            } else {
                alert("Wrong username or password");
            }
        }}).catch((err) => {console.log(err)});
    }
    
    render() {
        return (
            <div>
                <h1>Login</h1>
                <label htmlFor="userLogin">Username:</label>
                <br/>
                <input id="userLogin" type="text" placeholder="username" onChange={this.handleChangeUsername} />
                <br/>
                <label htmlFor="passLogin">Password:</label>
                <br/>
                <input id="passLogin" type="password" placeholder="password" onChange={this.handleChangePassword} />
                <br/>
                <button id="login" onClick={this.verifyUser}>Login</button>
            </div>
        );
    }
}