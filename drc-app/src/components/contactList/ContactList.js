import React from 'react';
import axios from "axios";

export class ContactList extends React.Component {
    deleteContact = (id) => {
        axios.delete("https://proiectdrc-ghawst.c9users.io:8081/delete-contact", {id});
        console.log(id);
    }
    
    render(){
        return(
            <div>
                <h1>{this.props.title}</h1>
                <div id="container">
                    {this.props.source.map((contact, index) => {
                       return <div className="contact" key={index}>{contact.lastName} {contact.firstName} - {contact.email} - {contact.phone} <button className="delete" onClick={() => this.deleteContact(contact.id)}>X</button></div> 
                    })}
                </div>
            </div>
        );
    }
}