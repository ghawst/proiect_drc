import React from "react";
import axios from "axios";

export class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state={};
        this.state.username = "";
        this.state.password = "";
        this.state.firstName = "";
        this.state.lastName = "";
        this.state.email = "";
    }
    
    handleChangeUsername = (event) => {
        this.setState({
            username: event.target.value
        })
    }
    
    handleChangePassword = (event) => {
        this.setState({
            password: event.target.value
        })
    }
    
    handleChangeFirstName = (event) => {
        this.setState({
            firstName: event.target.value
        })
    }
    
    handleChangeLastName = (event) => {
        this.setState({
            lastName: event.target.value
        })
    }
    
    handleChangeEmail = (event) => {
        this.setState({
            email: event.target.value
        })
    }
    
    addUser = () => {
        let user = {
            username: this.state.username,
            password: this.state.password,
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            email: this.state.email
        }
        axios.post("https://proiectdrc-ghawst.c9users.io:8081/register", user).then((res) => {if(res.status === 200){this.props.userAdded(user);}}).catch((err) => {console.log(err)});
    }
    
    render() {
        return (
            <div>
                <h1>Register</h1>
                <label htmlFor="userRegister">Username:</label>
                <br/>
                <input id="userRegister" type="text" placeholder="username" onChange={this.handleChangeUsername} />
                <br/>
                <label htmlFor="passRegister">Password:</label>
                <br/>
                <input id="passRegister" type="password" placeholder="password" onChange={this.handleChangePassword} />
                <br/>
                <label htmlFor="fNameRegister">First Name:</label>
                <br/>
                <input id="fNameRegister" type="text" placeholder="First Name" onChange={this.handleChangeFirstName} />
                <br/>
                <label htmlFor="lNameRegister">Last Name:</label>
                <br/>
                <input id="lNameRegister" type="text" placeholder="Last Name" onChange={this.handleChangeLastName} />
                <br/>
                <label htmlFor="emailRegister">Email:</label>
                <br/>
                <input id="emailRegister" type="email" placeholder="Email" onChange={this.handleChangeEmail} />
                <br/>
                <button id="register" onClick={this.addUser}>Register</button>
            </div>
        );
    }
}