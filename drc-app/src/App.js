import React, { Component } from 'react';
import './App.css';
import {AddContact} from "./components/addContact/AddContact";
import {ContactList} from './components/contactList/ContactList';
import {Register} from './components/register/Register';
import {Login} from './components/login/Login';

const fetch = require("node-fetch");

class App extends Component {
  
  constructor(props){
    super(props);
    this.state = {};
    this.state.contacts = [];
    this.state.user = {};
    this.state.userContacts = [];
    window.currentUser = "@";
  }
  
  onContactAdded = (contact) => {
    let contacts = this.state.contacts;
    contacts.push(contact);
    this.setState({
      contacts: contacts
    });
  }
  
  onUserAdded = (user) => {
    this.setState({
      user: user
    });
  }
  
  componentWillMount(){
    const url = "https://proiectdrc-ghawst.c9users.io:8081/get-all-contacts";
    fetch(url).then((res) => {
      return res.json();
    }).then((contacts) =>{
      this.setState({
        contacts: contacts
      })
    });
  }
  
  getUserContacts = () => {
    const url = "https://proiectdrc-ghawst.c9users.io:8081/get-user-contacts/" + window.currentUser;
    fetch(url).then((res) => {
      return res.json();
    }).then((contacts) =>{
      this.setState({
        userContacts: contacts
      })
    });
  }
  
  render() {
    return (
      <React.Fragment>
        <Register userAdded={this.onUserAdded} />
        <Login loggedIn={this.getUserContacts} />
        <AddContact contactAdded={this.getUserContacts} />
        <ContactList title="Contact List" source={this.state.userContacts} />
      </React.Fragment>
    );
  }
}

export default App;